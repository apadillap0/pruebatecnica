package com.neoris.pruebatecnica.controlador;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.neoris.pruebatecnica.dto.MovimientoDto;
import com.neoris.pruebatecnica.dto.ReporteDto;
import com.neoris.pruebatecnica.servicios.IMovimientoServicio;
import com.neoris.pruebatecnica.tipos.TipoMovimiento;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.ArrayList;

import java.util.Date;
import java.util.List;
import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.*;

@WebMvcTest(MovimientoControlador.class)
public class MovimientoControladorTest {


    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private IMovimientoServicio movimientoServicio;


    @Test
    public void testCrearMovimiento() throws Exception {
        MovimientoDto movimientoDto = new MovimientoDto();
        movimientoDto.setFecha(new Date());
        movimientoDto.setTipoMovimiento(TipoMovimiento.DEBITO);
        movimientoDto.setValor(10100D);
        movimientoDto.setIdentificacion("1045704903");
        movimientoDto.setNumeroCuenta("12345");
        movimientoDto.setClave("2566");

        MovimientoDto respuesta = new MovimientoDto();
        respuesta.setId(4L);
        respuesta.setFecha(new Date());
        respuesta.setTipoMovimiento(TipoMovimiento.DEBITO);
        respuesta.setValor(10100D);
        respuesta.setSaldo(189900D);
        respuesta.setEstado(true);
        respuesta.setNumeroCuenta("12345");
        respuesta.setSaldoInicial(200000D);
        respuesta.setIdentificacion("1045704903");
        respuesta.setCliente("ALVARO PADILLA");

        when(movimientoServicio.guardarMovimiento(any())).thenReturn(respuesta);

        mockMvc.perform(post("/movimientos")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(movimientoDto)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(4))
                .andExpect(jsonPath("$.tipoMovimiento").value("DEBITO"))
                .andExpect(jsonPath("$.valor").value(10100.0))
                .andExpect(jsonPath("$.saldo").value(189900.0))
                .andExpect(jsonPath("$.estado").value(true))
                .andExpect(jsonPath("$.numeroCuenta").value("12345"))
                .andExpect(jsonPath("$.saldoInicial").value(200000.0))
                .andExpect(jsonPath("$.identificacion").value("1045704903"))
                .andExpect(jsonPath("$.cliente").value("ALVARO PADILLA"));
    }

}
