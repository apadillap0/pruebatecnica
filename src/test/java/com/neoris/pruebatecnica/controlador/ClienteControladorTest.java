package com.neoris.pruebatecnica.controlador;


import com.neoris.pruebatecnica.dto.ClienteDto;
import com.neoris.pruebatecnica.servicios.IClienteServicio;
import com.neoris.pruebatecnica.tipos.TipoGeneroPersona;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.Matchers.is;


import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@WebMvcTest(ClienteControlador.class)
public class ClienteControladorTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private IClienteServicio clienteServicio;

    @Test
    public void testObtenerTodosClientes() throws Exception {
        List<ClienteDto> clientes = new ArrayList<>();
        when(clienteServicio.obtenerTodosLosClientes()).thenReturn(clientes);
        mockMvc.perform(get("/clientes"))
                .andExpect(status().isOk())
                .andExpect(content().json("[]"));
    }

    @Test
    public void testObtenerClientePorId() throws Exception {
        ClienteDto clienteDto = new ClienteDto();
        clienteDto.setId(1L);
        clienteDto.setNombre("ALVARO PADILLA");
        clienteDto.setGenero(TipoGeneroPersona.MASCULINO);
        clienteDto.setEdad(31);
        clienteDto.setIdentificacion("1045704903");
        clienteDto.setDireccion("CARRERA 14 #6-46 BARRIO GAIRA");
        clienteDto.setTelefono("3046613922");
        clienteDto.setContrasena("2566");
        clienteDto.setEstado(Boolean.TRUE);

        when(clienteServicio.obtenerClientePorId(anyLong())).thenReturn(clienteDto);

        mockMvc.perform(get("/clientes/{id-cliente}", 1L))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.nombre", is("ALVARO PADILLA")))
                .andExpect(jsonPath("$.genero", is("MASCULINO")))
                .andExpect(jsonPath("$.edad", is(31)))
                .andExpect(jsonPath("$.identificacion", is("1045704903")))
                .andExpect(jsonPath("$.direccion", is("CARRERA 14 #6-46 BARRIO GAIRA")))
                .andExpect(jsonPath("$.telefono", is("3046613922")))
                .andExpect(jsonPath("$.contrasena", is("2566")))
                .andExpect(jsonPath("$.estado", is(Boolean.TRUE)));
    }

    @Test
    public void testObtenerClientePorIdentificacion() throws Exception {
        ClienteDto cliente = new ClienteDto();
        cliente.setId(1L);
        cliente.setNombre("ALVARO PADILLA");
        cliente.setGenero(TipoGeneroPersona.MASCULINO);
        cliente.setEdad(31);
        cliente.setIdentificacion("1045704903");
        cliente.setDireccion("CARRERA 14 #6-46 BARRIO GAIRA");
        cliente.setTelefono("3046613922");
        cliente.setContrasena("2566");
        cliente.setEstado(Boolean.TRUE);

        when(clienteServicio.obtenerClientePorIdentificacion(anyString())).thenReturn(cliente);

        mockMvc.perform(get("/clientes/identificacion/1045704903"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.nombre", is("ALVARO PADILLA")))
                .andExpect(jsonPath("$.genero", is("MASCULINO")))
                .andExpect(jsonPath("$.edad", is(31)))
                .andExpect(jsonPath("$.identificacion", is("1045704903")))
                .andExpect(jsonPath("$.direccion", is("CARRERA 14 #6-46 BARRIO GAIRA")))
                .andExpect(jsonPath("$.telefono", is("3046613922")))
                .andExpect(jsonPath("$.contrasena", is("2566")))
                .andExpect(jsonPath("$.estado", is(Boolean.TRUE)));
    }

    @Test
    public void testCrearCliente() throws Exception {
        ClienteDto cliente = new ClienteDto();
        cliente.setId(1L);
        cliente.setNombre("ALVARO PADILLA");
        cliente.setGenero(TipoGeneroPersona.MASCULINO);
        cliente.setEdad(31);
        cliente.setIdentificacion("1045704903");
        cliente.setDireccion("CARRERA 14 #6-46 BARRIO GAIRA");
        cliente.setTelefono("3046613922");
        cliente.setContrasena("2566");
        cliente.setEstado(Boolean.TRUE);

        when(clienteServicio.guardarCliente(any())).thenReturn(cliente);

        mockMvc.perform(post("/clientes")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"nombre\":\"ALVARO PADILLA\",\"genero\":\"MASCULINO\",\"edad\":31,\"identificacion\":\"1045704903\",\"direccion\":\"CARRERA 14 #6-46 BARRIO GAIRA\",\"telefono\":\"3046613922\",\"contrasena\":\"2566\"}"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"id\":1,\"nombre\":\"ALVARO PADILLA\",\"genero\":\"MASCULINO\",\"edad\":31,\"identificacion\":\"1045704903\",\"direccion\":\"CARRERA 14 #6-46 BARRIO GAIRA\",\"telefono\":\"3046613922\",\"contrasena\":\"2566\",\"estado\":true}"));
    }

    @Test
    public void testActualizarCliente() throws Exception {
        ClienteDto cliente = new ClienteDto();
        cliente.setId(1L);
        cliente.setNombre("ALVARO PADILLA");
        cliente.setGenero(TipoGeneroPersona.MASCULINO);
        cliente.setEdad(31);
        cliente.setIdentificacion("1045704903");
        cliente.setDireccion("CARRERA 14 #6-46 BARRIO GAIRA");
        cliente.setTelefono("3046613922");
        cliente.setContrasena("2566");
        cliente.setEstado(Boolean.TRUE);
        // Configurar el cliente
        when(clienteServicio.actualizarCliente(any())).thenReturn(cliente);
        mockMvc.perform(put("/clientes")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"nombre\":\"ALVARO PADILLA\",\"genero\":\"MASCULINO\",\"edad\":31,\"identificacion\":\"1045704903\",\"direccion\":\"CARRERA 14 #6-46 BARRIO GAIRA\",\"telefono\":\"3046613922\",\"contrasena\":\"2566\"}"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"id\":1,\"nombre\":\"ALVARO PADILLA\",\"genero\":\"MASCULINO\",\"edad\":31,\"identificacion\":\"1045704903\",\"direccion\":\"CARRERA 14 #6-46 BARRIO GAIRA\",\"telefono\":\"3046613922\",\"contrasena\":\"2566\",\"estado\":true}"));
    }

    @Test
    public void testDesactivarCliente() throws Exception {
        when(clienteServicio.desactivarCliente(anyLong())).thenReturn(Boolean.TRUE);
        mockMvc.perform(patch("/clientes/1"))
                .andExpect(status().isOk());
    }

    @Test
    public void testRemoverCliente() throws Exception {
        when(clienteServicio.removerCliente(anyLong())).thenReturn(true);
        mockMvc.perform(delete("/clientes/1"))
                .andExpect(status().isOk());
    }
}
