package com.neoris.pruebatecnica.controlador;

import com.neoris.pruebatecnica.dto.CuentaDto;
import com.neoris.pruebatecnica.servicios.ICuentaServicio;
import com.neoris.pruebatecnica.tipos.TipoCuenta;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;



import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@WebMvcTest(CuentaControlador.class)
public class CuentaControladorTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ICuentaServicio cuentaServicio;

    @Test
    public void obtenerTodasLasCuentas() throws Exception {
        List<CuentaDto> cuentas = new ArrayList<>();
        when(cuentaServicio.obtenerTodosLasCuentas()).thenReturn(cuentas);
        mockMvc.perform(get("/cuentas"))
                .andExpect(status().isOk())
                .andExpect(content().json("[]"));
    }

    @Test
    public void testObtenerCuentaPorId() throws Exception {
        CuentaDto cuenta = new CuentaDto();
        cuenta.setId(1L);
        cuenta.setNumeroCuenta("1234");
        cuenta.setTipoCuenta(TipoCuenta.AHORRO);
        cuenta.setSaldoInicial(100D);
        cuenta.setIdentificacion("1045704903");
        cuenta.setCliente("ALVARO PADILLA");
        cuenta.setEstado(Boolean.TRUE);


        when(cuentaServicio.obtenerCuentaPorId(1L)).thenReturn(cuenta);

        mockMvc.perform(get("/cuentas/1"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"id\":1,\"numeroCuenta\":\"1234\",\"tipoCuenta\":\"AHORRO\",\"saldoInicial\":100.0,\"identificacion\":\"1045704903\",\"cliente\":\"ALVARO PADILLA\",\"estado\":true}"));
    }

    @Test
    public void testObtenerCuentaPorNumero() throws Exception {
        CuentaDto cuenta = new CuentaDto();
        cuenta.setId(1L);
        cuenta.setNumeroCuenta("1234");
        cuenta.setTipoCuenta(TipoCuenta.AHORRO);
        cuenta.setSaldoInicial(100D);
        cuenta.setIdentificacion("1045704903");
        cuenta.setCliente("ALVARO PADILLA");
        cuenta.setEstado(Boolean.TRUE);

        when(cuentaServicio.obtenerCuentaPorNumeroCuenta("1234")).thenReturn(cuenta);

        mockMvc.perform(get("/cuentas/numerocuenta/1234"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"id\":1,\"numeroCuenta\":\"1234\",\"tipoCuenta\":\"AHORRO\",\"saldoInicial\":100.0,\"identificacion\":\"1045704903\",\"cliente\":\"ALVARO PADILLA\",\"estado\":true}"));
    }

    @Test
    public void testCrearCuenta() throws Exception {
        CuentaDto cuenta = new CuentaDto();
        cuenta.setId(1L);
        cuenta.setNumeroCuenta("1234");
        cuenta.setTipoCuenta(TipoCuenta.AHORRO);
        cuenta.setSaldoInicial(100D);
        cuenta.setIdentificacion("1045704903");
        cuenta.setCliente("ALVARO PADILLA");
        cuenta.setEstado(Boolean.TRUE);

        when(cuentaServicio.guardarCuenta(any())).thenReturn(cuenta);

        mockMvc.perform(post("/cuentas")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"numeroCuenta\":\"12345\",\"tipoCuenta\":\"AHORRO\",\"saldoInicial\":100,\"identificacion\":\"1045704903\"}"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"id\":1,\"numeroCuenta\":\"1234\",\"tipoCuenta\":\"AHORRO\",\"saldoInicial\":100.0,\"estado\":true,\"identificacion\":\"1045704903\",\"cliente\":\"ALVARO PADILLA\"}"));
    }

    @Test
    public void testActualizarCuenta() throws Exception {
        CuentaDto cuenta = new CuentaDto();
        cuenta.setId(1L);
        cuenta.setNumeroCuenta("1234");
        cuenta.setTipoCuenta(TipoCuenta.AHORRO);
        cuenta.setSaldoInicial(100D);
        cuenta.setIdentificacion("1045704903");
        cuenta.setCliente("ALVARO PADILLA");
        cuenta.setEstado(Boolean.TRUE);

        when(cuentaServicio.actualizarCuenta(any())).thenReturn(cuenta);

        mockMvc.perform(put("/cuentas")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"numeroCuenta\":\"1234\",\"tipoCuenta\":\"AHORRO\",\"saldoInicial\":100,\"identificacion\":\"1045704903\"}"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"id\":1,\"numeroCuenta\":\"1234\",\"tipoCuenta\":\"AHORRO\",\"saldoInicial\":100.0,\"estado\":true,\"identificacion\":\"1045704903\",\"cliente\":\"ALVARO PADILLA\"}"));
    }

    @Test
    public void testDesactivarCuenta() throws Exception {
        when(cuentaServicio.desactivarCuenta(anyLong())).thenReturn(Boolean.TRUE);
        mockMvc.perform(patch("/cuentas/1"))
                .andExpect(status().isOk());
    }

    @Test
    public void testRemoverCuenta() throws Exception {
        when(cuentaServicio.removerCuenta(anyLong())).thenReturn(true);
        mockMvc.perform(delete("/cuentas/1"))
                .andExpect(status().isOk());
    }

}
