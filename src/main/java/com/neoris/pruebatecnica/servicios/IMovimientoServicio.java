package com.neoris.pruebatecnica.servicios;


import com.neoris.pruebatecnica.dto.MovimientoDto;
import com.neoris.pruebatecnica.dto.ReporteDto;

import java.util.Date;
import java.util.List;

public interface IMovimientoServicio {

    public MovimientoDto guardarMovimiento(MovimientoDto movimientoDto);

    public List<ReporteDto> ObtenerReportePorCliente(String numeroDeCuenta,Date fechaInicio, Date fechaFin);

}
