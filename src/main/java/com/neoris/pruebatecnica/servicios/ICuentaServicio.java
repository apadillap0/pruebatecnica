package com.neoris.pruebatecnica.servicios;

import com.neoris.pruebatecnica.dto.CuentaDto;

import java.util.List;

public interface ICuentaServicio {

    public CuentaDto guardarCuenta(CuentaDto cuentaDto);

    public CuentaDto actualizarCuenta(CuentaDto cuentaDto);

    public Boolean desactivarCuenta(Long idCuenta);

    public Boolean removerCuenta(Long idCuenta);

    public List<CuentaDto> obtenerTodosLasCuentas();

    public CuentaDto obtenerCuentaPorId(Long idCuenta);

    public CuentaDto obtenerCuentaPorNumeroCuenta(String numeroCuenta);
}
