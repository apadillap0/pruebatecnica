package com.neoris.pruebatecnica.servicios.implementaciones.constantes;

public class ClienteConstante {

    public static final String IDENTIFICACION = "Identificacion";
    public static final String NOMBRE = "Nombre";
    public static final String DIRECCION = "Direccion";
    public static final String TELEFONO = "Telefono";

    public static final String GENERO = "Genero";

    public static final String EDAD = "Edad";

    public static final String CONTRASENA = "Contraseña";



}
