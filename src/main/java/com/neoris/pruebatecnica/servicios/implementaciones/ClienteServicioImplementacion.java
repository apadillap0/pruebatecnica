package com.neoris.pruebatecnica.servicios.implementaciones;

import com.neoris.pruebatecnica.dto.ClienteDto;
import com.neoris.pruebatecnica.entidades.Cliente;
import com.neoris.pruebatecnica.excepciones.NeorisExcepcion;
import com.neoris.pruebatecnica.repositorios.ClienteRepositorio;
import com.neoris.pruebatecnica.servicios.IClienteServicio;
import com.neoris.pruebatecnica.servicios.implementaciones.constantes.ApiCodigoConstante;
import com.neoris.pruebatecnica.servicios.implementaciones.constantes.ClienteConstante;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ClienteServicioImplementacion implements IClienteServicio {


    @Autowired
    private ClienteRepositorio clienteRepositorio;


    @Autowired
    private UtilidadesServicioImplementacion utilidadesServicioImplementacion;


    @Override
    public ClienteDto guardarCliente(ClienteDto clienteDto) {
        validarCliente(clienteDto);
        Optional<Cliente> clienteOptional = clienteRepositorio.findByIdentificacion(clienteDto.getIdentificacion());
        if (!clienteOptional.isPresent()) {
            Cliente cliente = generarCliente(clienteDto);
            clienteRepositorio.save(cliente);
            return generarClienteDto(cliente);
        }
        throw new NeorisExcepcion(
                ApiCodigoConstante.CLIENTE_YA_EXISTE.getDescripcion(),
                ApiCodigoConstante.CLIENTE_YA_EXISTE.getHttpStatus());
    }

    private void validarCliente (ClienteDto clienteDto){
        utilidadesServicioImplementacion.validarCampoStringRequerido(clienteDto.getNombre(), ClienteConstante.NOMBRE);
        utilidadesServicioImplementacion.validarCampoStringRequerido(clienteDto.getIdentificacion(), ClienteConstante.IDENTIFICACION);
        utilidadesServicioImplementacion.validarCampoStringRequerido(clienteDto.getDireccion(), ClienteConstante.DIRECCION);
        utilidadesServicioImplementacion.validarCampoStringRequerido(clienteDto.getTelefono(), ClienteConstante.TELEFONO);
        utilidadesServicioImplementacion.validarCampoEnteroRequerido(clienteDto.getEdad(), ClienteConstante.EDAD);
        utilidadesServicioImplementacion.validarCampoTipoRequerido(clienteDto.getGenero(),ClienteConstante.GENERO);
        utilidadesServicioImplementacion.validarCampoStringRequerido(clienteDto.getContrasena(),ClienteConstante.CONTRASENA);
    }

    private Cliente generarCliente(ClienteDto clienteDto){
        return Cliente.builder().id(clienteDto.getId()).nombre(clienteDto.getNombre())
                .genero(clienteDto.getGenero()).edad(clienteDto.getEdad())
                .identificacion(clienteDto.getIdentificacion()).direccion(clienteDto.getDireccion())
                .telefono(clienteDto.getTelefono()).contrasena(clienteDto.getContrasena()).estado(clienteDto.getEstado()).build();
    }

    private ClienteDto generarClienteDto(Cliente cliente){
        return ClienteDto.builder().id(cliente.getId()).nombre(cliente.getNombre())
                .genero(cliente.getGenero()).edad(cliente.getEdad())
                .identificacion(cliente.getIdentificacion()).direccion(cliente.getDireccion())
                .telefono(cliente.getTelefono()).contrasena(cliente.getContrasena()).estado(cliente.getEstado()).build();
    }



    @Override
    public ClienteDto actualizarCliente(ClienteDto clienteDto) {
        validarCliente(clienteDto);
        Optional<Cliente> clienteOptional = clienteRepositorio.findByIdentificacion(clienteDto.getIdentificacion());
        if (clienteOptional.isPresent()) {
            Cliente cliente = clienteOptional.get();
            cliente.setEdad(clienteDto.getEdad());
            cliente.setGenero(clienteDto.getGenero());
            cliente.setNombre(clienteDto.getNombre());
            cliente.setDireccion(clienteDto.getDireccion());
            cliente.setTelefono(clienteDto.getTelefono());
            clienteRepositorio.save(cliente);
            return generarClienteDto(cliente);
        }
        throw new NeorisExcepcion(
                ApiCodigoConstante.CLIENTE_NO_EXISTE.getDescripcion(),
                ApiCodigoConstante.CLIENTE_NO_EXISTE.getHttpStatus());
    }

    @Override
    public Boolean desactivarCliente(Long idCliente) {
        Boolean resultado = Boolean.FALSE;
        try {
            clienteRepositorio.desactivarCliente(idCliente);
            resultado = Boolean.TRUE;
        } catch (Exception e) {
            throw new NeorisExcepcion(
                    ApiCodigoConstante.CLIENTE_ERROR_DESACTIVAR.getDescripcion(),
                    ApiCodigoConstante.CLIENTE_ERROR_DESACTIVAR.getHttpStatus());
        }
        return resultado;
    }

    @Override
    public Boolean removerCliente(Long idCliente) {
        Boolean resultado = Boolean.FALSE;
        try {
            Cliente cliente = clienteRepositorio.getReferenceById(idCliente);
            clienteRepositorio.delete(cliente);
            resultado = Boolean.TRUE;
        } catch (Exception e) {
            throw new NeorisExcepcion(
                    ApiCodigoConstante.CLIENTE_ERROR_ELIMINAR.getDescripcion(),
                    ApiCodigoConstante.CLIENTE_ERROR_ELIMINAR.getHttpStatus());
        }
        return resultado;
    }

    @Override
    public List<ClienteDto> obtenerTodosLosClientes() {
        return clienteRepositorio.obtenerTodosLosCliente();
    }

    @Override
    public ClienteDto obtenerClientePorId(Long idCliente) {
        Optional<Cliente> clienteOptional = clienteRepositorio.findById(idCliente);
        if (clienteOptional.isPresent()) {
            return generarClienteDto(clienteOptional.get());
        }
        throw new NeorisExcepcion(
                ApiCodigoConstante.CLIENTE_NO_EXISTE.getDescripcion(),
                ApiCodigoConstante.CLIENTE_NO_EXISTE.getHttpStatus());
    }

    @Override
    public ClienteDto obtenerClientePorIdentificacion(String identificacion) {
        Optional<Cliente> clienteOptional = clienteRepositorio.findByIdentificacion(identificacion);
        if (clienteOptional.isPresent()) {
            return generarClienteDto(clienteOptional.get());
        }
        throw new NeorisExcepcion(
                ApiCodigoConstante.CLIENTE_NO_EXISTE.getDescripcion(),
                ApiCodigoConstante.CLIENTE_NO_EXISTE.getHttpStatus());
    }
}
