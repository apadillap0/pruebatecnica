package com.neoris.pruebatecnica.servicios.implementaciones;


import com.neoris.pruebatecnica.dto.MovimientoDto;
import com.neoris.pruebatecnica.dto.ReporteDto;
import com.neoris.pruebatecnica.entidades.Cliente;
import com.neoris.pruebatecnica.entidades.Cuenta;
import com.neoris.pruebatecnica.entidades.Movimiento;
import com.neoris.pruebatecnica.excepciones.NeorisExcepcion;
import com.neoris.pruebatecnica.repositorios.ClienteRepositorio;
import com.neoris.pruebatecnica.repositorios.CuentaRepositorio;
import com.neoris.pruebatecnica.repositorios.MovimientoRepositorio;
import com.neoris.pruebatecnica.servicios.IMovimientoServicio;
import com.neoris.pruebatecnica.servicios.implementaciones.constantes.ApiCodigoConstante;
import com.neoris.pruebatecnica.servicios.implementaciones.constantes.MovimientoConstante;
import com.neoris.pruebatecnica.tipos.TipoMovimiento;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class MovimientoServicioImplementacion implements IMovimientoServicio {

    @Autowired
    private ClienteRepositorio clienteRepositorio;

    @Autowired
    private CuentaRepositorio cuentaRepositorio;

    @Autowired
    private MovimientoRepositorio movimientoRepositorio;

    @Autowired
    private UtilidadesServicioImplementacion utilidadesServicioImplementacion;


    @Override
    public MovimientoDto guardarMovimiento(MovimientoDto movimientoDto) {
        validarMovimiento(movimientoDto);
        Optional<Cliente> clienteOptional = clienteRepositorio.findByIdentificacion(movimientoDto.getIdentificacion());
        if(clienteOptional.isPresent()) {
            Optional<Cuenta> cuentaOptional = cuentaRepositorio.findByNumeroCuenta(movimientoDto.getNumeroCuenta());
            if (cuentaOptional.isPresent()) {
                if(clienteOptional.get().getContrasena().equals(movimientoDto.getClave())) {
                    Double saldo = validarSaldoDeLaCuentaYGenerarSaldo(cuentaOptional.get(),movimientoDto.getTipoMovimiento(),movimientoDto.getValor());
                    Movimiento movimiento = generarMovimiento(movimientoDto,cuentaOptional.get(),saldo);
                    movimientoRepositorio.save(movimiento);
                    return generarMovimientoDto(movimiento);
                }
                throw new NeorisExcepcion(
                        ApiCodigoConstante.CLIENTE_NO_AUTORIZADO.getDescripcion(),
                        ApiCodigoConstante.CLIENTE_NO_AUTORIZADO.getHttpStatus());
            }
            throw new NeorisExcepcion(
                    ApiCodigoConstante.CUENTA_YA_EXISTE.getDescripcion(),
                    ApiCodigoConstante.CUENTA_YA_EXISTE.getHttpStatus());
        }
        throw new NeorisExcepcion(
                ApiCodigoConstante.CLIENTE_NO_EXISTE.getDescripcion(),
                ApiCodigoConstante.CLIENTE_NO_EXISTE.getHttpStatus());

    }

    @Override
    public List<ReporteDto> ObtenerReportePorCliente(String identificacion, Date fechaInicio, Date fechaFin) {
        return movimientoRepositorio.ObtenerReportePorCliente(identificacion,fechaInicio,fechaFin);
    }

    private void validarMovimiento(MovimientoDto movimientoDto){
        utilidadesServicioImplementacion.validarCampoStringRequerido(movimientoDto.getNumeroCuenta(), MovimientoConstante.NUMERO_CUENTA);
        utilidadesServicioImplementacion.validarCampoFechaRequerido(movimientoDto.getFecha(), MovimientoConstante.FECHA);
        utilidadesServicioImplementacion.validarCampoDecimalRequerido(movimientoDto.getValor(), MovimientoConstante.VALOR);
        utilidadesServicioImplementacion.validarCampoTipoRequerido(movimientoDto.getTipoMovimiento(), MovimientoConstante.TIPO_MOVIMIENTO);
        utilidadesServicioImplementacion.validarCampoStringRequerido(movimientoDto.getIdentificacion(), MovimientoConstante.IDENTIFICACION);
        utilidadesServicioImplementacion.validarCampoStringRequerido(movimientoDto.getClave(), MovimientoConstante.CLAVE);
    }

    private Double validarSaldoDeLaCuentaYGenerarSaldo(Cuenta cuenta, TipoMovimiento tipoMovimiento, Double valor){
        List<Movimiento> movimientos = movimientoRepositorio.findUltimoMovimientoByCuentaId(cuenta.getId());
        if(TipoMovimiento.DEBITO == tipoMovimiento){
            if(movimientos.isEmpty()){
                if(cuenta.getSaldoInicial() == 0 || (cuenta.getSaldoInicial() - valor) < 0){
                    throw new NeorisExcepcion(
                            ApiCodigoConstante.SALDO_NO_DISPONIBLE.getDescripcion(),
                            ApiCodigoConstante.SALDO_NO_DISPONIBLE.getHttpStatus());
                }
                return (cuenta.getSaldoInicial() - valor);
            }else{
                if(movimientos.get(0).getSaldo() == 0 || (movimientos.get(0).getSaldo() - valor) < 0){
                    throw new NeorisExcepcion(
                            ApiCodigoConstante.SALDO_NO_DISPONIBLE.getDescripcion(),
                            ApiCodigoConstante.SALDO_NO_DISPONIBLE.getHttpStatus());
                }
                return (movimientos.get(0).getSaldo() - valor);
            }
        }else{
            if(movimientos.isEmpty()){
                return (cuenta.getSaldoInicial() + valor);
            }else{
                return (movimientos.get(0).getSaldo() + valor);
            }
        }
    }

    private Movimiento generarMovimiento(MovimientoDto movimientoDto, Cuenta cuenta, Double saldo){
        return Movimiento.builder().id(movimientoDto.getId()).fecha(movimientoDto.getFecha())
                .tipoMovimiento(movimientoDto.getTipoMovimiento()).valor(movimientoDto.getValor())
                .saldo(saldo).cuenta(cuenta).build();
    }

    private MovimientoDto generarMovimientoDto(Movimiento movimiento){
        return MovimientoDto.builder().id(movimiento.getId()).fecha(movimiento.getFecha())
                .tipoMovimiento(movimiento.getTipoMovimiento()).valor(movimiento.getValor())
                .saldo(movimiento.getSaldo()).estado(movimiento.getEstado())
                .numeroCuenta(movimiento.getCuenta().getNumeroCuenta()).saldoInicial(movimiento.getCuenta().getSaldoInicial())
                .cliente(movimiento.getCuenta().getCliente().getNombre()).identificacion(movimiento.getCuenta().getCliente().getIdentificacion()).build();
    }
}
