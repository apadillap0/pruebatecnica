package com.neoris.pruebatecnica.servicios.implementaciones.constantes;

public class MovimientoConstante {

    public static final String FECHA = "Fecha del movimiento";

    public static final String TIPO_MOVIMIENTO = "Tipo de movimiento";

    public static final String VALOR = "Valor del movimiento";

    public static final String NUMERO_CUENTA = "Numero de la cuenta";

    public static final String IDENTIFICACION = "Identificacion del cliente";

    public static final String CLAVE = "Clave del cliente";



}
