package com.neoris.pruebatecnica.servicios.implementaciones.constantes;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
@AllArgsConstructor
public enum ApiCodigoConstante {


    PARAMETRO_NULL_O_VACIO("El parámetro viene Vacío => ", HttpStatus.PRECONDITION_FAILED),
    CLIENTE_ERROR_DESACTIVAR("Ha ocurrido un error al desactivar el cliente", HttpStatus.INTERNAL_SERVER_ERROR),
    CLIENTE_ERROR_ELIMINAR("Ha ocurrido un error al eliminar el cliente", HttpStatus.INTERNAL_SERVER_ERROR),
    CLIENTE_YA_EXISTE("Cliente con esa identificacion ya se encuentra registrado", HttpStatus.PRECONDITION_FAILED),
    CLIENTE_NO_EXISTE("Cliente con esa identificacion no se encuentra registrado", HttpStatus.PRECONDITION_FAILED),
    CUENTA_YA_EXISTE("Cuenta con ese numero ya se encuentra registrado", HttpStatus.PRECONDITION_FAILED),
    CUENTA_NO_EXISTE("Cuenta con ese numero no se encuentra registrado", HttpStatus.PRECONDITION_FAILED),
    CUENTA_ERROR_DESACTIVAR("Ha ocurrido un error al desactivar la cuenta", HttpStatus.INTERNAL_SERVER_ERROR),
    CUENTA_ERROR_ELIMINAR("Ha ocurrido un error al eliminar la cuenta", HttpStatus.INTERNAL_SERVER_ERROR),
    CLIENTE_NO_AUTORIZADO("Clave incorrecta, por favor verifiquela", HttpStatus.UNAUTHORIZED),
    SALDO_NO_DISPONIBLE("Saldo no disponible", HttpStatus.PRECONDITION_FAILED);

    private String descripcion;

    private HttpStatus httpStatus;
}
