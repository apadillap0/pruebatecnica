package com.neoris.pruebatecnica.servicios.implementaciones;

import com.neoris.pruebatecnica.dto.ClienteDto;
import com.neoris.pruebatecnica.dto.CuentaDto;
import com.neoris.pruebatecnica.entidades.Cliente;
import com.neoris.pruebatecnica.entidades.Cuenta;
import com.neoris.pruebatecnica.excepciones.NeorisExcepcion;
import com.neoris.pruebatecnica.repositorios.ClienteRepositorio;
import com.neoris.pruebatecnica.repositorios.CuentaRepositorio;
import com.neoris.pruebatecnica.servicios.ICuentaServicio;
import com.neoris.pruebatecnica.servicios.implementaciones.constantes.ApiCodigoConstante;
import com.neoris.pruebatecnica.servicios.implementaciones.constantes.ClienteConstante;
import com.neoris.pruebatecnica.servicios.implementaciones.constantes.CuentaConstante;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CuentaServicioImplementacion implements ICuentaServicio {

    @Autowired
    private ClienteRepositorio clienteRepositorio;

    @Autowired
    private CuentaRepositorio cuentaRepositorio;

    @Autowired
    private UtilidadesServicioImplementacion utilidadesServicioImplementacion;


    @Override
    public CuentaDto guardarCuenta(CuentaDto cuentaDto) {
        validarCuenta(cuentaDto);
        Optional<Cliente> clienteOptional = clienteRepositorio.findByIdentificacion(cuentaDto.getIdentificacion());
        if(clienteOptional.isPresent()) {
            Optional<Cuenta> cuentaOptional = cuentaRepositorio.findByNumeroCuenta(cuentaDto.getNumeroCuenta());
            if (!cuentaOptional.isPresent()) {
                Cuenta cuenta = generarCuenta(cuentaDto, clienteOptional.get());
                cuentaRepositorio.save(cuenta);
                return generarCuentaDto(cuenta);
            }
            throw new NeorisExcepcion(
                    ApiCodigoConstante.CUENTA_YA_EXISTE.getDescripcion(),
                    ApiCodigoConstante.CUENTA_YA_EXISTE.getHttpStatus());
        }
        throw new NeorisExcepcion(
                ApiCodigoConstante.CLIENTE_NO_EXISTE.getDescripcion(),
                ApiCodigoConstante.CLIENTE_NO_EXISTE.getHttpStatus());
    }


    private void validarCuenta (CuentaDto cuentaDto){
        utilidadesServicioImplementacion.validarCampoStringRequerido(cuentaDto.getNumeroCuenta(), CuentaConstante.NUMERO_CUENTA);
        utilidadesServicioImplementacion.validarCampoStringRequerido(cuentaDto.getIdentificacion(), CuentaConstante.IDENTIFICACION);
        utilidadesServicioImplementacion.validarCampoDecimalRequerido(cuentaDto.getSaldoInicial(), CuentaConstante.SALDO_INICIAL);
        utilidadesServicioImplementacion.validarCampoTipoRequerido(cuentaDto.getTipoCuenta(),CuentaConstante.TIPO_CUENTA);
    }

    private Cuenta generarCuenta(CuentaDto cuentaDto, Cliente cliente){
        return Cuenta.builder().id(cuentaDto.getId()).numeroCuenta(cuentaDto.getNumeroCuenta())
                .tipoCuenta(cuentaDto.getTipoCuenta()).saldoInicial(cuentaDto.getSaldoInicial()).cliente(cliente).build();

    }

    private CuentaDto generarCuentaDto(Cuenta cuenta){
        return CuentaDto.builder().id(cuenta.getId()).numeroCuenta(cuenta.getNumeroCuenta())
                .tipoCuenta(cuenta.getTipoCuenta()).estado(cuenta.getEstado())
                .saldoInicial(cuenta.getSaldoInicial()).identificacion(cuenta.getCliente().getIdentificacion())
                .cliente(cuenta.getCliente().getNombre()).build();
    }

    @Override
    public CuentaDto actualizarCuenta(CuentaDto cuentaDto) {
        validarCuenta(cuentaDto);
        Optional<Cuenta> cuentaOptional = cuentaRepositorio.findByNumeroCuenta(cuentaDto.getNumeroCuenta());
        if (cuentaOptional.isPresent()) {
            Cuenta cuenta = cuentaOptional.get();
            cuenta.setTipoCuenta(cuentaDto.getTipoCuenta());
            cuenta.setSaldoInicial(cuentaDto.getSaldoInicial());
            cuentaRepositorio.save(cuenta);
            return generarCuentaDto(cuenta);
        }
        throw new NeorisExcepcion(
                ApiCodigoConstante.CUENTA_NO_EXISTE.getDescripcion(),
                ApiCodigoConstante.CUENTA_NO_EXISTE.getHttpStatus());
    }

    @Override
    public Boolean desactivarCuenta(Long idCuenta) {
        Boolean resultado = Boolean.FALSE;
        try {
            cuentaRepositorio.desactivarCuenta(idCuenta);
            resultado = Boolean.TRUE;
        } catch (Exception e) {
            throw new NeorisExcepcion(
                    ApiCodigoConstante.CUENTA_ERROR_DESACTIVAR.getDescripcion(),
                    ApiCodigoConstante.CUENTA_ERROR_DESACTIVAR.getHttpStatus());
        }
        return resultado;
    }

    @Override
    public Boolean removerCuenta(Long idCuenta) {
        Boolean resultado = Boolean.FALSE;
        try {
            Cuenta cuenta = cuentaRepositorio.getReferenceById(idCuenta);
            cuentaRepositorio.delete(cuenta);
            resultado = Boolean.TRUE;
        } catch (Exception e) {
            throw new NeorisExcepcion(
                    ApiCodigoConstante.CUENTA_ERROR_ELIMINAR.getDescripcion(),
                    ApiCodigoConstante.CUENTA_ERROR_ELIMINAR.getHttpStatus());
        }
        return resultado;
    }

    @Override
    public List<CuentaDto> obtenerTodosLasCuentas() {
        return cuentaRepositorio.obtenerTodosLasCuentas();
    }

    @Override
    public CuentaDto obtenerCuentaPorId(Long idCuenta) {
        Optional<Cuenta> cuentaOptional = cuentaRepositorio.findById(idCuenta);
        if (cuentaOptional.isPresent()) {
            return generarCuentaDto(cuentaOptional.get());
        }
        throw new NeorisExcepcion(
                ApiCodigoConstante.CUENTA_NO_EXISTE.getDescripcion(),
                ApiCodigoConstante.CUENTA_NO_EXISTE.getHttpStatus());
    }

    @Override
    public CuentaDto obtenerCuentaPorNumeroCuenta(String numeroCuenta) {
        Optional<Cuenta> cuentaOptional = cuentaRepositorio.findByNumeroCuenta(numeroCuenta);
        if (cuentaOptional.isPresent()) {
            return generarCuentaDto(cuentaOptional.get());
        }
        throw new NeorisExcepcion(
                ApiCodigoConstante.CUENTA_NO_EXISTE.getDescripcion(),
                ApiCodigoConstante.CUENTA_NO_EXISTE.getHttpStatus());
    }



}
