package com.neoris.pruebatecnica.servicios.implementaciones;

import com.neoris.pruebatecnica.excepciones.NeorisExcepcion;
import com.neoris.pruebatecnica.servicios.IUtilidadeServicio;
import com.neoris.pruebatecnica.servicios.implementaciones.constantes.ApiCodigoConstante;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class UtilidadesServicioImplementacion implements IUtilidadeServicio {
    @Override
    public void validarCampoStringRequerido(String valor, String nombreCampo) {
        if (StringUtils.isBlank(valor)) {
            throw new NeorisExcepcion(
                    ApiCodigoConstante.PARAMETRO_NULL_O_VACIO.getDescripcion().concat(nombreCampo),
                    ApiCodigoConstante.PARAMETRO_NULL_O_VACIO.getHttpStatus());

        }
    }

    @Override
    public void validarCampoEnteroRequerido(Integer valor, String NombreCampo) {
        if (valor == null) {
            throw new NeorisExcepcion(
                    ApiCodigoConstante.PARAMETRO_NULL_O_VACIO.getDescripcion().concat(NombreCampo),
                    ApiCodigoConstante.PARAMETRO_NULL_O_VACIO.getHttpStatus());
        }
    }

    @Override
    public void validarCampoDecimalRequerido(Double valor, String NombreCampo) {
        if (valor == null) {
            throw new NeorisExcepcion(
                    ApiCodigoConstante.PARAMETRO_NULL_O_VACIO.getDescripcion().concat(NombreCampo),
                    ApiCodigoConstante.PARAMETRO_NULL_O_VACIO.getHttpStatus());
        }
    }

    @Override
    public void validarCampoFechaRequerido(Date valor, String NombreCampo) {
        if (valor == null) {
            throw new NeorisExcepcion(
                    ApiCodigoConstante.PARAMETRO_NULL_O_VACIO.getDescripcion().concat(NombreCampo),
                    ApiCodigoConstante.PARAMETRO_NULL_O_VACIO.getHttpStatus());
        }
    }

    @Override
    public <T extends Enum<T>> void validarCampoTipoRequerido(T tipo, String NombreTipo) {
        if (tipo == null)  {
            throw new NeorisExcepcion(
                    ApiCodigoConstante.PARAMETRO_NULL_O_VACIO.getDescripcion().concat(NombreTipo),
                    ApiCodigoConstante.PARAMETRO_NULL_O_VACIO.getHttpStatus());
        }
    }
}
