package com.neoris.pruebatecnica.servicios;

import com.neoris.pruebatecnica.dto.ClienteDto;
import com.neoris.pruebatecnica.entidades.Cliente;

import java.util.List;

public interface IClienteServicio {

    public ClienteDto guardarCliente(ClienteDto clienteDto);

    public ClienteDto actualizarCliente(ClienteDto clienteDto);

    public Boolean desactivarCliente(Long idCliente);

    public Boolean removerCliente(Long idCliente);

    public List<ClienteDto> obtenerTodosLosClientes();

    public ClienteDto obtenerClientePorId(Long idCliente);

    public ClienteDto obtenerClientePorIdentificacion(String identificacion);

}
