package com.neoris.pruebatecnica.servicios;

import java.util.Date;

public interface IUtilidadeServicio {

    public void validarCampoStringRequerido(String valor,String NombreCampo);

    public void validarCampoEnteroRequerido(Integer valor,String NombreCampo);

    public void validarCampoDecimalRequerido(Double valor,String NombreCampo);

    public void validarCampoFechaRequerido(Date valor, String NombreCampo);

    public  <T extends Enum<T>> void validarCampoTipoRequerido(T tipo,String NombreTipo);
}
