package com.neoris.pruebatecnica.advertencias;

import com.neoris.pruebatecnica.dto.Notificacion;
import com.neoris.pruebatecnica.excepciones.NeorisExcepcion;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class NeorisAdvertencia {

    @ExceptionHandler(NeorisExcepcion.class)
    public ResponseEntity<Notificacion> handle(NeorisExcepcion exception) {
        return ResponseEntity.status(exception.getEstatus()).body(
                Notificacion.builder().mensaje(exception.getMensaje()).build());
    }
}
