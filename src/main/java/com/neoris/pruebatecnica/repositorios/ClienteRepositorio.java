package com.neoris.pruebatecnica.repositorios;

import com.neoris.pruebatecnica.dto.ClienteDto;
import com.neoris.pruebatecnica.entidades.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


public interface ClienteRepositorio extends JpaRepository <Cliente,Long> {

    @Modifying
    @Transactional
    @Query("UPDATE Cliente c SET c.estado = false WHERE c.id = ?1")
    void desactivarCliente(Long id);

    Optional<Cliente> findByIdentificacion(String identificacion);

    @Query("SELECT NEW com.neoris.pruebatecnica.dto.ClienteDto(c.id, c.nombre, c.genero, c.edad, c.identificacion, c.direccion, c.telefono, c.contrasena, c.estado) FROM Cliente c")
    List<ClienteDto> obtenerTodosLosCliente();

}
