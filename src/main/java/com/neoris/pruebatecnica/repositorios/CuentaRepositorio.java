package com.neoris.pruebatecnica.repositorios;


import com.neoris.pruebatecnica.dto.ClienteDto;
import com.neoris.pruebatecnica.dto.CuentaDto;
import com.neoris.pruebatecnica.entidades.Cliente;
import com.neoris.pruebatecnica.entidades.Cuenta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface CuentaRepositorio extends JpaRepository<Cuenta,Long> {

    @Modifying
    @Transactional
    @Query("UPDATE Cuenta c SET c.estado = false WHERE c.id = ?1")
    void desactivarCuenta(Long id);

    Optional<Cuenta> findByNumeroCuenta(String numeroCuenta);

    @Query("SELECT NEW com.neoris.pruebatecnica.dto.CuentaDto(c.id, c.numeroCuenta, c.tipoCuenta, c.saldoInicial, c.estado, c.cliente.identificacion, c.cliente.nombre) FROM Cuenta c")
    List<CuentaDto> obtenerTodosLasCuentas();

}
