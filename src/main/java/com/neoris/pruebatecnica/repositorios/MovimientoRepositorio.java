package com.neoris.pruebatecnica.repositorios;

import com.neoris.pruebatecnica.dto.ReporteDto;
import com.neoris.pruebatecnica.entidades.Movimiento;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface MovimientoRepositorio extends JpaRepository<Movimiento,Long> {

    @Modifying
    @Transactional
    @Query("UPDATE Movimiento m SET m.estado = false WHERE m.id = ?1")
    void desactivarMovimiento(Long id);

    @Query("SELECT m FROM Movimiento m WHERE m.cuenta.id = ?1 and m.estado = true ORDER BY m.id DESC")
    List<Movimiento> findUltimoMovimientoByCuentaId(Long idCuenta);

    @Query("SELECT  NEW com.neoris.pruebatecnica.dto.ReporteDto(m.fecha, m.cuenta.cliente.nombre, m.cuenta.numeroCuenta, m.cuenta.tipoCuenta, m.cuenta.saldoInicial, m.estado, m.valor, m.saldo) FROM Movimiento m " +
            "WHERE m.cuenta.cliente.identificacion = :identificacion and (m.fecha BETWEEN :fechaInicio and :fechaFin)" +
            "ORDER BY m.cuenta.numeroCuenta, m.fecha")
    List<ReporteDto> ObtenerReportePorCliente(@Param("identificacion") String identificacion,
                                                     @Param("fechaInicio") Date fechaInicio,
                                                     @Param("fechaFin") Date fechaFin);

}
