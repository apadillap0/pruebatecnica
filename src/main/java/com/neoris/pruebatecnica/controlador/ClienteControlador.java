package com.neoris.pruebatecnica.controlador;

import com.neoris.pruebatecnica.controlador.constante.ClienteControladorConstante;
import com.neoris.pruebatecnica.dto.ClienteDto;
import com.neoris.pruebatecnica.servicios.IClienteServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(ClienteControladorConstante.CLIENTE)
public class ClienteControlador {


    @Autowired()
    private IClienteServicio clienteServicio;

    @GetMapping("")
    public ResponseEntity<List<ClienteDto>> obtenerTodosClientes(){
        return ResponseEntity.status(HttpStatus.OK)
                .body(clienteServicio.obtenerTodosLosClientes());
    }

    @GetMapping(ClienteControladorConstante.CLIENTE_POR_ID)
    public ResponseEntity<ClienteDto> obtenerClientePorId(@PathVariable(ClienteControladorConstante.CLIENTE_ID) Long idCliente){
        return ResponseEntity.status(HttpStatus.OK)
                .body(clienteServicio.obtenerClientePorId(idCliente));
    }

    @GetMapping(ClienteControladorConstante.CLIENTE_POR_IDENTIFICACION)
    public ResponseEntity<ClienteDto> obtenerClientePorId(@PathVariable(ClienteControladorConstante.CLIENTE_IDENTIFICACION) String identificacion){
        return ResponseEntity.status(HttpStatus.OK)
                .body(clienteServicio.obtenerClientePorIdentificacion(identificacion));
    }

    @PostMapping("")
    public ResponseEntity<ClienteDto> crearCliente(@RequestBody ClienteDto clienteDto){
        return ResponseEntity.status(HttpStatus.OK)
                .body(clienteServicio.guardarCliente(clienteDto));
    }

    @PutMapping("")
    public ResponseEntity<ClienteDto> actualizarCliente(@RequestBody ClienteDto clienteDto){
        return ResponseEntity.status(HttpStatus.OK)
                .body(clienteServicio.actualizarCliente(clienteDto));
    }

    @PatchMapping(ClienteControladorConstante.CLIENTE_POR_ID)
    public ResponseEntity<Boolean> desactivarCliente(@PathVariable(ClienteControladorConstante.CLIENTE_ID) Long idCliente){
        return ResponseEntity.status(HttpStatus.OK)
                .body(clienteServicio.desactivarCliente(idCliente));
    }

    @DeleteMapping(ClienteControladorConstante.CLIENTE_POR_ID)
    public ResponseEntity<Boolean> removerCliente(@PathVariable(ClienteControladorConstante.CLIENTE_ID) Long idCliente){
        return ResponseEntity.status(HttpStatus.OK)
                .body(clienteServicio.removerCliente(idCliente));
    }



}
