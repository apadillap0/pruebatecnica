package com.neoris.pruebatecnica.controlador.constante;

public class ClienteControladorConstante {

    public static final String CLIENTE_ID = "id-cliente";
    public static final String CLIENTE_IDENTIFICACION = "identificacion";
    public static final String CLIENTE = "/clientes";
    public static final String CLIENTE_POR_ID = "/{"+CLIENTE_ID+"}";
    public static final String CLIENTE_POR_IDENTIFICACION = "/identificacion/{"+CLIENTE_IDENTIFICACION+"}";

}
