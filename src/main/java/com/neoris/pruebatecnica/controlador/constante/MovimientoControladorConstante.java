package com.neoris.pruebatecnica.controlador.constante;

public class MovimientoControladorConstante {


    public static final String IDENTIFICACION = "identificacion";

    public static final String FECHA_INICIO = "fechaInicio";
    public static final String FECHA_FIN = "fechaFin";
    public static final String MOVIMIENTO = "/movimientos";
    public static final String REPORTE = "/reporte/{"+IDENTIFICACION+"}";


}
