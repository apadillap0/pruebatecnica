package com.neoris.pruebatecnica.controlador.constante;

public class CuentaControladorConstante {

    public static final String CUENTA_ID = "id-cuenta";
    public static final String NUMERO_CUENTA = "numeroCuenta";
    public static final String CUENTA = "/cuentas";
    public static final String CUENTA_POR_ID = "/{"+CUENTA_ID+"}";
    public static final String CUENTA_POR_NUMERO_CUENTA = "/numerocuenta/{"+NUMERO_CUENTA+"}";

}
