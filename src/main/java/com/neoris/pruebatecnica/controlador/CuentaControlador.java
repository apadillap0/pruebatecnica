package com.neoris.pruebatecnica.controlador;


import com.neoris.pruebatecnica.controlador.constante.CuentaControladorConstante;

import com.neoris.pruebatecnica.dto.CuentaDto;

import com.neoris.pruebatecnica.servicios.ICuentaServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(CuentaControladorConstante.CUENTA)
public class CuentaControlador {


    @Autowired()
    private ICuentaServicio cuentaServicio;

    @GetMapping("")
    public ResponseEntity<List<CuentaDto>> obtenerTodasLasCuentas(){
        return ResponseEntity.status(HttpStatus.OK)
                .body(cuentaServicio.obtenerTodosLasCuentas());
    }

    @GetMapping(CuentaControladorConstante.CUENTA_POR_ID)
    public ResponseEntity<CuentaDto> obtenerCuentaPorId(@PathVariable(CuentaControladorConstante.CUENTA_ID) Long idCuenta){
        return ResponseEntity.status(HttpStatus.OK)
                .body(cuentaServicio.obtenerCuentaPorId(idCuenta));
    }

    @GetMapping(CuentaControladorConstante.CUENTA_POR_NUMERO_CUENTA)
    public ResponseEntity<CuentaDto> obtenerCuentaPorNumero(@PathVariable(CuentaControladorConstante.NUMERO_CUENTA) String numeroCuenta){
        return ResponseEntity.status(HttpStatus.OK)
                .body(cuentaServicio.obtenerCuentaPorNumeroCuenta(numeroCuenta));
    }

    @PostMapping("")
    public ResponseEntity<CuentaDto> crearCuenta(@RequestBody CuentaDto cuentaDto){
        return ResponseEntity.status(HttpStatus.OK)
                .body(cuentaServicio.guardarCuenta(cuentaDto));
    }

    @PutMapping("")
    public ResponseEntity<CuentaDto> actualizarCuenta(@RequestBody CuentaDto cuentaDto){
        return ResponseEntity.status(HttpStatus.OK)
                .body(cuentaServicio.actualizarCuenta(cuentaDto));
    }

    @PatchMapping(CuentaControladorConstante.CUENTA_POR_ID)
    public ResponseEntity<Boolean> desactivarCliente(@PathVariable(CuentaControladorConstante.CUENTA_ID) Long idCuenta){
        return ResponseEntity.status(HttpStatus.OK)
                .body(cuentaServicio.desactivarCuenta(idCuenta));
    }

    @DeleteMapping(CuentaControladorConstante.CUENTA_POR_ID)
    public ResponseEntity<Boolean> removerCliente(@PathVariable(CuentaControladorConstante.CUENTA_ID) Long idcuenta){
        return ResponseEntity.status(HttpStatus.OK)
                .body(cuentaServicio.removerCuenta(idcuenta));
    }



}
