package com.neoris.pruebatecnica.controlador;


import com.neoris.pruebatecnica.controlador.constante.CuentaControladorConstante;
import com.neoris.pruebatecnica.controlador.constante.MovimientoControladorConstante;
import com.neoris.pruebatecnica.dto.CuentaDto;
import com.neoris.pruebatecnica.dto.MovimientoDto;
import com.neoris.pruebatecnica.dto.ReporteDto;
import com.neoris.pruebatecnica.servicios.ICuentaServicio;
import com.neoris.pruebatecnica.servicios.IMovimientoServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(MovimientoControladorConstante.MOVIMIENTO)
public class MovimientoControlador {

    @Autowired()
    private IMovimientoServicio movimientoServicio;

    @PostMapping("")
    public ResponseEntity<MovimientoDto> crearMovimiento(@RequestBody MovimientoDto movimientoDto){
        return ResponseEntity.status(HttpStatus.OK)
                .body(movimientoServicio.guardarMovimiento(movimientoDto));
    }

    @GetMapping(MovimientoControladorConstante.REPORTE)
    public ResponseEntity<List<ReporteDto>> obtenerReportePorCuenta(@PathVariable(MovimientoControladorConstante.IDENTIFICACION) String identificacion,
                                                                    @RequestParam(value = MovimientoControladorConstante.FECHA_INICIO) @DateTimeFormat(pattern = "dd/MM/yyyy") Date fechaInicio,
                                                                    @RequestParam(value = MovimientoControladorConstante.FECHA_FIN) @DateTimeFormat(pattern = "dd/MM/yyyy") Date fechaFin){
        return ResponseEntity.status(HttpStatus.OK)
                .body(movimientoServicio.ObtenerReportePorCliente(identificacion,fechaInicio,fechaFin));
    }


}
