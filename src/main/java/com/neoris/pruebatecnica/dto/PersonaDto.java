package com.neoris.pruebatecnica.dto;

import com.neoris.pruebatecnica.tipos.TipoGeneroPersona;
import lombok.*;
import com.fasterxml.jackson.annotation.JsonInclude;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PersonaDto {

    private Long id;
    private String nombre;
    private TipoGeneroPersona genero;
    private  Integer edad;
    private  String identificacion;
    private  String direccion;
    private  String telefono;

    @Override
    public String toString() {
        return "PersonaDto{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", genero=" + genero +
                ", edad=" + edad +
                ", identificacion='" + identificacion + '\'' +
                ", direccion='" + direccion + '\'' +
                ", telefono='" + telefono + '\'' +
                '}';
    }
}
