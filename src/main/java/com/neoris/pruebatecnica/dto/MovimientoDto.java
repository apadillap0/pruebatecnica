package com.neoris.pruebatecnica.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.neoris.pruebatecnica.tipos.TipoCuenta;
import com.neoris.pruebatecnica.tipos.TipoMovimiento;
import lombok.*;

import java.util.Date;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Builder
public class MovimientoDto {


    private Long id;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date fecha;

    private TipoMovimiento tipoMovimiento;

    private Double valor;

    private Double saldo;

    private Boolean estado;

    private String numeroCuenta;

    private Double saldoInicial;

    private String identificacion;

    private String cliente;

    private String clave;

}
