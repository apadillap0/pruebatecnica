package com.neoris.pruebatecnica.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.neoris.pruebatecnica.tipos.TipoCuenta;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ReporteDto {

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    @JsonProperty("Fecha")
    private Date fecha;
    @JsonProperty("Cliente")
    private String cliente;
    @JsonProperty("Numero Cuenta")
    private String numeroCuenta;
    @JsonProperty("Tipo")
    private TipoCuenta tipoCuenta;
    @JsonProperty("Saldo Inicial")
    private Double saldoInicial;
    @JsonProperty("Estado")
    private Boolean estado;
    @JsonProperty("Movimiento")
    private Double valor;
    @JsonProperty("Saldo Disponible")
    private Double saldoDisponible;

}
