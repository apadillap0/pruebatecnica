package com.neoris.pruebatecnica.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.neoris.pruebatecnica.tipos.TipoCuenta;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CuentaDto {
    private Long id;
    private String numeroCuenta;
    private TipoCuenta tipoCuenta;
    private Double saldoInicial;
    private Boolean estado;
    private String identificacion;

    private String cliente;

}
