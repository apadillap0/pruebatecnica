package com.neoris.pruebatecnica.dto;


import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class Notificacion {

    private String mensaje;

}
