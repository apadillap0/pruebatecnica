package com.neoris.pruebatecnica.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.neoris.pruebatecnica.tipos.TipoGeneroPersona;
import lombok.*;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ClienteDto extends PersonaDto {
    private String contrasena;
    private Boolean estado;

    @Builder
    public ClienteDto(Long id, String nombre, TipoGeneroPersona genero, Integer edad, String identificacion, String direccion,
                   String telefono, String contrasena, Boolean estado) {
        super(id,nombre, genero, edad, identificacion, direccion, telefono);
        this.contrasena = contrasena;
        this.estado = estado;
    }

}
