package com.neoris.pruebatecnica.entidades;


import javax.persistence.*;
import java.util.Date;

import com.neoris.pruebatecnica.convertidor.TipoMovimientoConvertidor;
import com.neoris.pruebatecnica.tipos.TipoMovimiento;
import lombok.*;

@Entity
@Table(name = "movimientos")
@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Movimiento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "fecha", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date fecha;

    @Column(name = "tipo_movimiento", nullable = false)
    @Convert(converter = TipoMovimientoConvertidor.class)
    private TipoMovimiento tipoMovimiento;

    @Column(name = "valor", nullable = false)
    private Double valor;

    @Column(name = "saldo", nullable = false)
    private Double saldo;

    @Column(name = "estado", nullable = false)
    private Boolean estado;

    @ManyToOne
    private Cuenta cuenta;

    @PrePersist
    public void prepersistir() {
        if (this.estado == null) {
            this.estado = Boolean.TRUE;
        }
    }
}
