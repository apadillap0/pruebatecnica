package com.neoris.pruebatecnica.entidades;

import com.neoris.pruebatecnica.convertidor.TipoCuentaConvertidor;
import com.neoris.pruebatecnica.tipos.TipoCuenta;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "cuentas")
@Getter
@Setter
@AllArgsConstructor
@Builder
public class Cuenta {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "numero_cuenta", unique = true, nullable = false)
    private String numeroCuenta;

    @Column(name = "tipo_cuenta", nullable = false)
    @Convert(converter = TipoCuentaConvertidor.class)
    private TipoCuenta tipoCuenta;

    @Column(name = "saldo_inicial", nullable = false)
    private Double saldoInicial;

    @Column(name = "estado", nullable = false)
    private Boolean estado;

    @ManyToOne
    private Cliente cliente;

    public Cuenta() {
    }

    @PrePersist
    public void prepersistir() {
        if (this.estado == null) {
            this.estado = Boolean.TRUE;
        }
    }
}
