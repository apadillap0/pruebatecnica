package com.neoris.pruebatecnica.entidades;

import com.neoris.pruebatecnica.tipos.TipoGeneroPersona;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "clientes")
@Getter
public class Cliente extends Persona{


    @Column(name = "contrasena")
    private String contrasena;

    @Column(name = "estado")
    private Boolean estado;

    @Builder
    public Cliente(Long id,String nombre, TipoGeneroPersona genero, Integer edad, String identificacion, String direccion,
                   String telefono, String contrasena, Boolean estado) {
        super(id,nombre, genero, edad, identificacion, direccion, telefono);
        this.contrasena = contrasena;
        this.estado = estado;
    }

    public Cliente() {
        super();
    }

    @PrePersist
    public void prepersistir() {
        if (this.estado == null) {
            this.estado = Boolean.TRUE;
        }
    }
}
