package com.neoris.pruebatecnica.entidades;

import com.neoris.pruebatecnica.convertidor.TipoCuentaConvertidor;
import com.neoris.pruebatecnica.convertidor.TipoGeneroPersonaConvertidor;
import com.neoris.pruebatecnica.tipos.TipoGeneroPersona;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;


@Getter
@Setter
@MappedSuperclass
public class Persona {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "nombre")
    private  String nombre;

    @Column(name = "genero")
    @Convert(converter = TipoGeneroPersonaConvertidor.class)
    private TipoGeneroPersona genero;

    @Column(name = "edad")
    private  Integer edad;

    @Column(name = "identificacion", unique = true)
    private  String identificacion;

    @Column(name = "direccion")
    private  String direccion;

    @Column(name = "telefono")
    private  String telefono;

    public Persona() {

    }

    public Persona(Long id, String nombre, TipoGeneroPersona genero, Integer edad, String identificacion, String direccion, String telefono) {
        this.id = id;
        this.nombre = nombre;
        this.genero = genero;
        this.edad = edad;
        this.identificacion = identificacion;
        this.direccion = direccion;
        this.telefono = telefono;
    }
}
