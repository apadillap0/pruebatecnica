package com.neoris.pruebatecnica.excepciones;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
public class NeorisExcepcion extends RuntimeException{

    private final HttpStatus estatus;

    private final String mensaje;

    public NeorisExcepcion(String mensaje, HttpStatus estatus) {
        this.estatus = estatus;
        this.mensaje = mensaje;
    }


}
