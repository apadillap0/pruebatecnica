package com.neoris.pruebatecnica.tipos;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TipoCuenta {

    CORRIENTE("CORRIENTE"),
    AHORRO("AHORRO");

    private  String value;
}
