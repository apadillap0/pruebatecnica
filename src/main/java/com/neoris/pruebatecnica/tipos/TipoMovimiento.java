package com.neoris.pruebatecnica.tipos;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TipoMovimiento {

    DEBITO("DEBITO"),
    CREDITO("CREDITO");

    private  String value;
}
