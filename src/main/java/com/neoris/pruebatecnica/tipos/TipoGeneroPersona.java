package com.neoris.pruebatecnica.tipos;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TipoGeneroPersona {

    MASCULINO("MASCULINO"),
    FEMENINO("FEMENINO");

    private  String value;
}
