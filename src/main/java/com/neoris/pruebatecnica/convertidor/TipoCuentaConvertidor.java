package com.neoris.pruebatecnica.convertidor;

import com.neoris.pruebatecnica.tipos.TipoCuenta;

import javax.persistence.AttributeConverter;

public class TipoCuentaConvertidor extends ConvertidorBasico implements AttributeConverter<TipoCuenta, String> {


    @Override
    public String convertToDatabaseColumn(TipoCuenta value) {
        return convertToColumn(value);
    }
    @Override
    public TipoCuenta convertToEntityAttribute(String value) {
        return (TipoCuenta) convertToAttribute(TipoCuenta.class, value);
    }

}
