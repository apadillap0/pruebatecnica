package com.neoris.pruebatecnica.convertidor;


import com.neoris.pruebatecnica.tipos.TipoMovimiento;

import javax.persistence.AttributeConverter;

public class TipoMovimientoConvertidor extends ConvertidorBasico implements AttributeConverter<TipoMovimiento, String> {


    @Override
    public String convertToDatabaseColumn(TipoMovimiento value) {
        return convertToColumn(value);
    }
    @Override
    public TipoMovimiento convertToEntityAttribute(String value) {
        return (TipoMovimiento) convertToAttribute(TipoMovimiento.class, value);
    }

}
