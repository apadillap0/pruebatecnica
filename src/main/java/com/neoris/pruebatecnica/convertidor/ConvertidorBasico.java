package com.neoris.pruebatecnica.convertidor;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public abstract  class ConvertidorBasico {

    public String convertToColumn(Object o) {
        if (o == null) {
            return null;
        }
        String val = null;
        Class < ? > clzz = o.getClass();
        Method method = null;
        try {
            method = clzz.getDeclaredMethod("getValue");
            val = (String) method.invoke(o);
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException |
                 InvocationTargetException e1) {
            e1.printStackTrace();
        }
        return val;
    }

    public Object convertToAttribute(Class < ? > cls, String value) {
        if (value == null) {
            return null;
        }
        try {
            for (Object obj: getEnumValues(cls)) {
                Class < ? > clz = obj.getClass();
                Method method = clz.getDeclaredMethod("getValue");
                String vle = (String) method.invoke(obj);
                if (vle.equals(value)) {
                    return obj;
                }
            }
            throw new IllegalArgumentException("ShortName [" + value + "] not supported to " + cls.getName());
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | InvocationTargetException | NoSuchFieldException e1) {
            e1.printStackTrace();
        }
        return null;
    }
    private static < E extends Enum > E[] getEnumValues(Class < ? > cls)
            throws NoSuchFieldException, IllegalAccessException {
        Field f = cls.getDeclaredField("$VALUES");
        f.setAccessible(true);
        Object o = f.get(null);
        return (E[]) o;
    }


}
