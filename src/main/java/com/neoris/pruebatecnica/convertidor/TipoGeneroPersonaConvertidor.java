package com.neoris.pruebatecnica.convertidor;


import com.neoris.pruebatecnica.tipos.TipoGeneroPersona;

import javax.persistence.AttributeConverter;

public class TipoGeneroPersonaConvertidor extends ConvertidorBasico implements AttributeConverter<TipoGeneroPersona, String> {

    @Override
    public String convertToDatabaseColumn(TipoGeneroPersona value) {
        return convertToColumn(value);
    }
    @Override
    public TipoGeneroPersona convertToEntityAttribute(String value) {
        return (TipoGeneroPersona) convertToAttribute(TipoGeneroPersona.class, value);
    }
}
