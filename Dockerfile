FROM openjdk:8-jdk-alpine
WORKDIR /app
EXPOSE 8000
COPY build/libs/pruebatecnica-0.0.1-SNAPSHOT.jar /app/app.jar
CMD ["java", "-jar", "app.jar"]

